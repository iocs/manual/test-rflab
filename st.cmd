epicsEnvSet("AS_TOP", "/tmp")
epicsEnvSet("IOCNAME", "LLRF:Ctrl-IOC-01")
epicsEnvSet("IOCDIR", "LLRF_Ctrl-IOC-01")
epicsEnvSet("LLRF_P", "LLRF")
epicsEnvSet("LLRF_R", "")
epicsEnvSet("LLRF_DIG_R_1", "DIG1")
epicsEnvSet("LLRF_RFM_R_1", "RFM1")
epicsEnvSet("LLRF_SLOT_1", "9")
epicsEnvSet("TSELPV", "OCTOPUS-010:RFS-EVR-101:EvtACnt-I.TIME")


require essioc

## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load LLRF
require sis8300llrf, 5.13.1+0
require llrfsystem, 3.11.1+0

iocshLoad("$(llrfsystem_DIR)/llrfsystem.iocsh", "LPSEN=0, LSPEN1=0")

dbl > PVs.list
## For commands to be run after iocInit, use the function afterInit()

# Call iocInit to start the IOC
iocInit()
date
